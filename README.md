# Tracking Poachers using BLE Beacons

To use the Ardunio uno:

- Install the Arduino IDE
- Install the library SoftwareSerial.
- Follow the connections below run the code
- Open Serial Monitor and select 9600 Baud rate

HM-10 connections

HM-10  -> Arduino
GND -> GND
VCC -> 3.7v
RX  -> D10 
TX -> D11


SIM900A connections

SIM900a  -> Arduino
GND -> GND
VCC -> 5v
RX  ->  D5
TX -> D6
State -> D4

