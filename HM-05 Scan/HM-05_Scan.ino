#include <SoftwareSerial.h>

SoftwareSerial BTSerial(10, 11); // RX | TX

int count=0;
char val;

void setup()
{
  pinMode(9, OUTPUT);  // this pin will pull the HC-05 pin 34 (key pin) HIGH to switch module to AT mode
  digitalWrite(9, HIGH);
  Serial.begin(9600);
  Serial.println("Enter AT commands:");
  BTSerial.begin(38400);  // HC-05 default speed in AT command mod  e
  Serial.println("AT");
  BTSerial.println("AT"); // Test HC-05 is in AT mode, should print OK
  delay(1000);
  //Serial.println("AT+ORGL");
  //BTSerial.println("AT+ORGL"); //Restore back to factory settings
  delay(1000);
  Serial.println("AT+INIT"); //
  BTSerial.println("AT+INIT");
  delay(1000);
  Serial.println("AT+ROLE=1");
  BTSerial.println("AT+ROLE=1"); //Sets  to Master
  delay(1000);
  Serial.println("AT+INQM=1,10000\r\n");  
  BTSerial.println("AT+INQM=1,10000,48");
  delay(1000);
  BTSerial.println("AT+INQ"); //scan for bluetooth signal
  delay(1000);
  //Serial.println("OK");
  BTSerial.println("AT+INQ");
  
}


void loop()
{
  // Keep reading from HC-05 and send to Arduino Serial Monitor
  if (BTSerial.available())
   {
    Serial.write(BTSerial.read());
    val = Serial.read();
   }

  // Keep reading from Arduino Serial Monitor and send to HC-05
  if (Serial.available())
    {
      BTSerial.write(Serial.read());
      val = Serial.read();
      
    }
    
//    
//    if (val == 'OK')
//    {
//      BTSerial.println("AT+INQ");
//    }
    

    
}
